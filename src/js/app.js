"use strict"

document.addEventListener('click', mpWidget);

function mpWidget(event) {
    const widget = document.querySelector('[data-widget]');
    if (event.target.closest('[data-widget-open]')) {
        widget.classList.add('mp_widget--open');
    }
    else if (event.target.closest('[data-widget-close]')) {
        widget.classList.remove('mp_widget--open');
    }
    else {
        widget.classList.remove('mp_widget--open');
    }
}
